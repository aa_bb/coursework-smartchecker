﻿namespace CourseWork_tmp1
{
    partial class StudentResultForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataViewer = new System.Windows.Forms.DataGridView();
            this.colTest = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colBasicAnswer = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colAnswerClass = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colStudentProgramAnswer = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colResult = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataViewer)).BeginInit();
            this.SuspendLayout();
            // 
            // dataViewer
            // 
            this.dataViewer.AllowUserToAddRows = false;
            this.dataViewer.AllowUserToDeleteRows = false;
            this.dataViewer.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataViewer.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colTest,
            this.colBasicAnswer,
            this.colAnswerClass,
            this.colStudentProgramAnswer,
            this.colResult});
            this.dataViewer.Location = new System.Drawing.Point(11, 18);
            this.dataViewer.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dataViewer.Name = "dataViewer";
            this.dataViewer.ReadOnly = true;
            this.dataViewer.RowHeadersWidth = 51;
            this.dataViewer.RowTemplate.Height = 28;
            this.dataViewer.Size = new System.Drawing.Size(690, 324);
            this.dataViewer.TabIndex = 0;
            // 
            // colTest
            // 
            this.colTest.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.colTest.HeaderText = "Тест";
            this.colTest.MinimumWidth = 6;
            this.colTest.Name = "colTest";
            this.colTest.ReadOnly = true;
            // 
            // colBasicAnswer
            // 
            this.colBasicAnswer.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.colBasicAnswer.HeaderText = "Базовый ответ";
            this.colBasicAnswer.MinimumWidth = 6;
            this.colBasicAnswer.Name = "colBasicAnswer";
            this.colBasicAnswer.ReadOnly = true;
            // 
            // colAnswerClass
            // 
            this.colAnswerClass.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.colAnswerClass.HeaderText = "Класс ответа";
            this.colAnswerClass.MinimumWidth = 6;
            this.colAnswerClass.Name = "colAnswerClass";
            this.colAnswerClass.ReadOnly = true;
            // 
            // colStudentProgramAnswer
            // 
            this.colStudentProgramAnswer.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.colStudentProgramAnswer.HeaderText = "Ответ студента";
            this.colStudentProgramAnswer.MinimumWidth = 6;
            this.colStudentProgramAnswer.Name = "colStudentProgramAnswer";
            this.colStudentProgramAnswer.ReadOnly = true;
            // 
            // colResult
            // 
            this.colResult.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.colResult.HeaderText = "Вердикт";
            this.colResult.MinimumWidth = 6;
            this.colResult.Name = "colResult";
            this.colResult.ReadOnly = true;
            // 
            // StudentResultForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(711, 360);
            this.Controls.Add(this.dataViewer);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "StudentResultForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            ((System.ComponentModel.ISupportInitialize)(this.dataViewer)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataViewer;
        private System.Windows.Forms.DataGridViewTextBoxColumn colTest;
        private System.Windows.Forms.DataGridViewTextBoxColumn colBasicAnswer;
        private System.Windows.Forms.DataGridViewTextBoxColumn colAnswerClass;
        private System.Windows.Forms.DataGridViewTextBoxColumn colStudentProgramAnswer;
        private System.Windows.Forms.DataGridViewTextBoxColumn colResult;
    }
}
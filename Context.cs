﻿using System.Data.Entity;

namespace CourseWork_tmp1
{
    public class Context : DbContext
    {
        public Context() : base(nameOrConnectionString: "Default") { }

        public DbSet<Model.Test> Tests { get; set; }
        public DbSet<Model.TaskInfo> TaskInfos { get; set; }
        public DbSet<Model.Pattern> Patterns { get; set; }
        public DbSet<Model.AnswerClass> AnswerClasses { get; set; }
        public DbSet<Model.Task> Tasks { get; set; }
        public DbSet<Model.Plugin> Plugins { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("public");
            modelBuilder.Entity<Model.Test>().ToTable("test");
            modelBuilder.Entity<Model.TaskInfo>().ToTable("task_info");
            modelBuilder.Entity<Model.AnswerClass>().ToTable("answer_class");
            modelBuilder.Entity<Model.Pattern>().ToTable("pattern");
            modelBuilder.Entity<Model.Task>().ToTable("task");
            modelBuilder.Entity<Model.Plugin>().ToTable("plugin");

            modelBuilder.Entity<Model.Task>()
            .HasKey(t => new { t.TaskInfoId, t.TestId });

            modelBuilder.Entity<Model.Pattern>().HasOptional(t => t.NextOuterPattern).WithMany().HasForeignKey(t => t.NextOuterPatternId);
            modelBuilder.Entity<Model.Pattern>().HasOptional(t => t.NextInnerPattern).WithMany().HasForeignKey(t => t.NextInnerPatternId);
        }
    }
}

﻿namespace CourseWork_tmp1
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.задачаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.создатьНовуюЗадачуToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.сохранитьЗадачиВФайлToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.загрузитьЗадачиИзФайлаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.проверитьРешениеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.менеджерПлагиновToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.конструкторToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.gvTaskManager = new System.Windows.Forms.DataGridView();
            this.btnAddTask = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gvTaskManager)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.задачаToolStripMenuItem,
            this.проверитьРешениеToolStripMenuItem,
            this.менеджерПлагиновToolStripMenuItem,
            this.конструкторToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(5, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(711, 30);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // задачаToolStripMenuItem
            // 
            this.задачаToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.создатьНовуюЗадачуToolStripMenuItem,
            this.сохранитьЗадачиВФайлToolStripMenuItem,
            this.загрузитьЗадачиИзФайлаToolStripMenuItem});
            this.задачаToolStripMenuItem.Name = "задачаToolStripMenuItem";
            this.задачаToolStripMenuItem.Size = new System.Drawing.Size(72, 26);
            this.задачаToolStripMenuItem.Text = "Задачи";
            // 
            // создатьНовуюЗадачуToolStripMenuItem
            // 
            this.создатьНовуюЗадачуToolStripMenuItem.Name = "создатьНовуюЗадачуToolStripMenuItem";
            this.создатьНовуюЗадачуToolStripMenuItem.Size = new System.Drawing.Size(288, 26);
            this.создатьНовуюЗадачуToolStripMenuItem.Text = "Создать новую задачу";
            this.создатьНовуюЗадачуToolStripMenuItem.Click += new System.EventHandler(this.СоздатьНовуюЗадачуToolStripMenuItem_Click);
            // 
            // сохранитьЗадачиВФайлToolStripMenuItem
            // 
            this.сохранитьЗадачиВФайлToolStripMenuItem.Name = "сохранитьЗадачиВФайлToolStripMenuItem";
            this.сохранитьЗадачиВФайлToolStripMenuItem.Size = new System.Drawing.Size(288, 26);
            this.сохранитьЗадачиВФайлToolStripMenuItem.Text = "Сохранить задачи в файл...";
            this.сохранитьЗадачиВФайлToolStripMenuItem.Click += new System.EventHandler(this.сохранитьЗадачиВФайлToolStripMenuItem_Click);
            // 
            // загрузитьЗадачиИзФайлаToolStripMenuItem
            // 
            this.загрузитьЗадачиИзФайлаToolStripMenuItem.Name = "загрузитьЗадачиИзФайлаToolStripMenuItem";
            this.загрузитьЗадачиИзФайлаToolStripMenuItem.Size = new System.Drawing.Size(288, 26);
            this.загрузитьЗадачиИзФайлаToolStripMenuItem.Text = "Загрузить задачи из файла...";
            this.загрузитьЗадачиИзФайлаToolStripMenuItem.Click += new System.EventHandler(this.загрузитьЗадачиИзФайлаToolStripMenuItem_Click);
            // 
            // проверитьРешениеToolStripMenuItem
            // 
            this.проверитьРешениеToolStripMenuItem.Name = "проверитьРешениеToolStripMenuItem";
            this.проверитьРешениеToolStripMenuItem.Size = new System.Drawing.Size(167, 26);
            this.проверитьРешениеToolStripMenuItem.Text = "Проверить решения";
            this.проверитьРешениеToolStripMenuItem.Click += new System.EventHandler(this.проверитьРешениеToolStripMenuItem_Click);
            // 
            // менеджерПлагиновToolStripMenuItem
            // 
            this.менеджерПлагиновToolStripMenuItem.Name = "менеджерПлагиновToolStripMenuItem";
            this.менеджерПлагиновToolStripMenuItem.Size = new System.Drawing.Size(167, 26);
            this.менеджерПлагиновToolStripMenuItem.Text = "Менеджер плагинов";
            this.менеджерПлагиновToolStripMenuItem.Click += new System.EventHandler(this.менеджерПлагиновToolStripMenuItem_Click);
            // 
            // конструкторToolStripMenuItem
            // 
            this.конструкторToolStripMenuItem.Name = "конструкторToolStripMenuItem";
            this.конструкторToolStripMenuItem.Size = new System.Drawing.Size(110, 26);
            this.конструкторToolStripMenuItem.Text = "Конструктор";
            this.конструкторToolStripMenuItem.Click += new System.EventHandler(this.конструкторToolStripMenuItem_Click);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // gvTaskManager
            // 
            this.gvTaskManager.AllowUserToAddRows = false;
            this.gvTaskManager.AllowUserToDeleteRows = false;
            this.gvTaskManager.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gvTaskManager.Dock = System.Windows.Forms.DockStyle.Top;
            this.gvTaskManager.Location = new System.Drawing.Point(0, 30);
            this.gvTaskManager.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gvTaskManager.Name = "gvTaskManager";
            this.gvTaskManager.ReadOnly = true;
            this.gvTaskManager.RowHeadersWidth = 51;
            this.gvTaskManager.RowTemplate.Height = 28;
            this.gvTaskManager.Size = new System.Drawing.Size(711, 299);
            this.gvTaskManager.TabIndex = 3;
            this.gvTaskManager.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gvTaskManager_CellClick);
            // 
            // btnAddTask
            // 
            this.btnAddTask.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnAddTask.Location = new System.Drawing.Point(0, 329);
            this.btnAddTask.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnAddTask.Name = "btnAddTask";
            this.btnAddTask.Size = new System.Drawing.Size(711, 31);
            this.btnAddTask.TabIndex = 4;
            this.btnAddTask.Text = "Создать новую задачу";
            this.btnAddTask.UseVisualStyleBackColor = true;
            this.btnAddTask.Click += new System.EventHandler(this.btnAddTask_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(711, 360);
            this.Controls.Add(this.btnAddTask);
            this.Controls.Add(this.gvTaskManager);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SmartChecker";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gvTaskManager)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem задачаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem создатьНовуюЗадачуToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem проверитьРешениеToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem менеджерПлагиновToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.DataGridView gvTaskManager;
        private System.Windows.Forms.Button btnAddTask;
        private System.Windows.Forms.ToolStripMenuItem сохранитьЗадачиВФайлToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem загрузитьЗадачиИзФайлаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem конструкторToolStripMenuItem;
    }
}


﻿namespace CourseWork_tmp1
{
    partial class PluginManagerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataViewer = new System.Windows.Forms.DataGridView();
            this.colPluginName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colLexems = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colStatus = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.colActiveBtn = new System.Windows.Forms.DataGridViewButtonColumn();
            this.btnLoadPlugin = new System.Windows.Forms.Button();
            this.txtPluginPath = new System.Windows.Forms.TextBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.dataViewer)).BeginInit();
            this.SuspendLayout();
            // 
            // dataViewer
            // 
            this.dataViewer.AllowUserToAddRows = false;
            this.dataViewer.AllowUserToDeleteRows = false;
            this.dataViewer.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataViewer.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colPluginName,
            this.colLexems,
            this.colStatus,
            this.colActiveBtn});
            this.dataViewer.Location = new System.Drawing.Point(11, 54);
            this.dataViewer.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dataViewer.Name = "dataViewer";
            this.dataViewer.ReadOnly = true;
            this.dataViewer.RowHeadersWidth = 51;
            this.dataViewer.RowTemplate.Height = 28;
            this.dataViewer.Size = new System.Drawing.Size(690, 296);
            this.dataViewer.TabIndex = 0;
            this.dataViewer.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataViewer_CellClick);
            // 
            // colPluginName
            // 
            this.colPluginName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.colPluginName.HeaderText = "Название плагина";
            this.colPluginName.MinimumWidth = 6;
            this.colPluginName.Name = "colPluginName";
            this.colPluginName.ReadOnly = true;
            // 
            // colLexems
            // 
            this.colLexems.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.colLexems.HeaderText = "Доступные лексемы";
            this.colLexems.MinimumWidth = 6;
            this.colLexems.Name = "colLexems";
            this.colLexems.ReadOnly = true;
            // 
            // colStatus
            // 
            this.colStatus.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.colStatus.HeaderText = "Доступно";
            this.colStatus.MinimumWidth = 6;
            this.colStatus.Name = "colStatus";
            this.colStatus.ReadOnly = true;
            this.colStatus.Width = 78;
            // 
            // colActiveBtn
            // 
            this.colActiveBtn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.colActiveBtn.HeaderText = "Отключить/Включить";
            this.colActiveBtn.MinimumWidth = 6;
            this.colActiveBtn.Name = "colActiveBtn";
            this.colActiveBtn.ReadOnly = true;
            // 
            // btnLoadPlugin
            // 
            this.btnLoadPlugin.Location = new System.Drawing.Point(513, 10);
            this.btnLoadPlugin.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnLoadPlugin.Name = "btnLoadPlugin";
            this.btnLoadPlugin.Size = new System.Drawing.Size(179, 29);
            this.btnLoadPlugin.TabIndex = 2;
            this.btnLoadPlugin.Text = "Загрузить плагин";
            this.btnLoadPlugin.UseVisualStyleBackColor = true;
            this.btnLoadPlugin.Click += new System.EventHandler(this.btnLoadPlugin_Click);
            // 
            // txtPluginPath
            // 
            this.txtPluginPath.Location = new System.Drawing.Point(19, 14);
            this.txtPluginPath.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtPluginPath.Name = "txtPluginPath";
            this.txtPluginPath.Size = new System.Drawing.Size(475, 22);
            this.txtPluginPath.TabIndex = 3;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // PluginManagerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(711, 360);
            this.Controls.Add(this.txtPluginPath);
            this.Controls.Add(this.btnLoadPlugin);
            this.Controls.Add(this.dataViewer);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PluginManagerForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Менеджер плагинов";
            ((System.ComponentModel.ISupportInitialize)(this.dataViewer)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataViewer;
        private System.Windows.Forms.Button btnLoadPlugin;
        private System.Windows.Forms.TextBox txtPluginPath;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPluginName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colLexems;
        private System.Windows.Forms.DataGridViewCheckBoxColumn colStatus;
        private System.Windows.Forms.DataGridViewButtonColumn colActiveBtn;
    }
}
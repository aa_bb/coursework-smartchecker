﻿namespace CourseWork_tmp1
{
    partial class TaskEditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblTaskName = new System.Windows.Forms.Label();
            this.lblTestDirPath = new System.Windows.Forms.Label();
            this.dataViewer = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dataViewer)).BeginInit();
            this.SuspendLayout();
            // 
            // lblTaskName
            // 
            this.lblTaskName.AutoSize = true;
            this.lblTaskName.Font = new System.Drawing.Font("Microsoft YaHei UI", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblTaskName.Location = new System.Drawing.Point(2, 7);
            this.lblTaskName.Name = "lblTaskName";
            this.lblTaskName.Size = new System.Drawing.Size(62, 31);
            this.lblTaskName.TabIndex = 0;
            this.lblTaskName.Text = "text";
            // 
            // lblTestDirPath
            // 
            this.lblTestDirPath.AutoSize = true;
            this.lblTestDirPath.Font = new System.Drawing.Font("Microsoft YaHei UI", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblTestDirPath.Location = new System.Drawing.Point(4, 37);
            this.lblTestDirPath.MaximumSize = new System.Drawing.Size(580, 48);
            this.lblTestDirPath.Name = "lblTestDirPath";
            this.lblTestDirPath.Size = new System.Drawing.Size(50, 26);
            this.lblTestDirPath.TabIndex = 1;
            this.lblTestDirPath.Text = "text";
            // 
            // dataViewer
            // 
            this.dataViewer.AllowUserToAddRows = false;
            this.dataViewer.AllowUserToDeleteRows = false;
            this.dataViewer.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataViewer.Location = new System.Drawing.Point(8, 74);
            this.dataViewer.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dataViewer.Name = "dataViewer";
            this.dataViewer.RowHeadersWidth = 51;
            this.dataViewer.RowTemplate.Height = 28;
            this.dataViewer.Size = new System.Drawing.Size(575, 243);
            this.dataViewer.TabIndex = 2;
            this.dataViewer.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataViewer_CellClick);
            this.dataViewer.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.dataViewer_CellValidating);
            // 
            // TaskEditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(594, 396);
            this.Controls.Add(this.dataViewer);
            this.Controls.Add(this.lblTestDirPath);
            this.Controls.Add(this.lblTaskName);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "TaskEditForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            ((System.ComponentModel.ISupportInitialize)(this.dataViewer)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblTaskName;
        private System.Windows.Forms.Label lblTestDirPath;
        private System.Windows.Forms.DataGridView dataViewer;
    }
}
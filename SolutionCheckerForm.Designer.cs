﻿namespace CourseWork_tmp1
{
    partial class SolutionCheckerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtSolutionsPath = new System.Windows.Forms.TextBox();
            this.btnSelect = new System.Windows.Forms.Button();
            this.btnStartChecking = new System.Windows.Forms.Button();
            this.dataViewer = new System.Windows.Forms.DataGridView();
            this.colStudentName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colTaskName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colResult = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.testingResultDataRow = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataViewer)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(144, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Папка с решениями:";
            // 
            // txtSolutionsPath
            // 
            this.txtSolutionsPath.Location = new System.Drawing.Point(161, 15);
            this.txtSolutionsPath.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtSolutionsPath.Name = "txtSolutionsPath";
            this.txtSolutionsPath.Size = new System.Drawing.Size(424, 22);
            this.txtSolutionsPath.TabIndex = 1;
            // 
            // btnSelect
            // 
            this.btnSelect.Location = new System.Drawing.Point(602, 10);
            this.btnSelect.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnSelect.Name = "btnSelect";
            this.btnSelect.Size = new System.Drawing.Size(98, 31);
            this.btnSelect.TabIndex = 2;
            this.btnSelect.Text = "Выбрать...";
            this.btnSelect.UseVisualStyleBackColor = true;
            this.btnSelect.Click += new System.EventHandler(this.btnSelect_Click);
            // 
            // btnStartChecking
            // 
            this.btnStartChecking.Location = new System.Drawing.Point(234, 46);
            this.btnStartChecking.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnStartChecking.Name = "btnStartChecking";
            this.btnStartChecking.Size = new System.Drawing.Size(264, 27);
            this.btnStartChecking.TabIndex = 3;
            this.btnStartChecking.Text = "Начать проверку";
            this.btnStartChecking.UseVisualStyleBackColor = true;
            this.btnStartChecking.Click += new System.EventHandler(this.btnStartChecking_Click);
            // 
            // dataViewer
            // 
            this.dataViewer.AllowUserToAddRows = false;
            this.dataViewer.AllowUserToDeleteRows = false;
            this.dataViewer.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataViewer.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colStudentName,
            this.colTaskName,
            this.colResult,
            this.testingResultDataRow});
            this.dataViewer.Location = new System.Drawing.Point(16, 85);
            this.dataViewer.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dataViewer.Name = "dataViewer";
            this.dataViewer.ReadOnly = true;
            this.dataViewer.RowHeadersWidth = 51;
            this.dataViewer.RowTemplate.Height = 28;
            this.dataViewer.Size = new System.Drawing.Size(684, 262);
            this.dataViewer.TabIndex = 4;
            this.dataViewer.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataViewer_CellClick);
            // 
            // colStudentName
            // 
            this.colStudentName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.colStudentName.HeaderText = "Имя студента";
            this.colStudentName.MinimumWidth = 6;
            this.colStudentName.Name = "colStudentName";
            this.colStudentName.ReadOnly = true;
            // 
            // colTaskName
            // 
            this.colTaskName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.colTaskName.HeaderText = "Название задачи";
            this.colTaskName.MinimumWidth = 6;
            this.colTaskName.Name = "colTaskName";
            this.colTaskName.ReadOnly = true;
            // 
            // colResult
            // 
            this.colResult.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.colResult.HeaderText = "Результат";
            this.colResult.MinimumWidth = 6;
            this.colResult.Name = "colResult";
            this.colResult.ReadOnly = true;
            // 
            // testingResultDataRow
            // 
            this.testingResultDataRow.HeaderText = "testingResultDataRow";
            this.testingResultDataRow.MinimumWidth = 6;
            this.testingResultDataRow.Name = "testingResultDataRow";
            this.testingResultDataRow.ReadOnly = true;
            this.testingResultDataRow.Visible = false;
            this.testingResultDataRow.Width = 125;
            // 
            // SolutionCheckerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(711, 360);
            this.Controls.Add(this.dataViewer);
            this.Controls.Add(this.btnStartChecking);
            this.Controls.Add(this.btnSelect);
            this.Controls.Add(this.txtSolutionsPath);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SolutionCheckerForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Проверка решений";
            ((System.ComponentModel.ISupportInitialize)(this.dataViewer)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtSolutionsPath;
        private System.Windows.Forms.Button btnSelect;
        private System.Windows.Forms.Button btnStartChecking;
        private System.Windows.Forms.DataGridView dataViewer;
        private System.Windows.Forms.DataGridViewTextBoxColumn colStudentName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colTaskName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colResult;
        private System.Windows.Forms.DataGridViewTextBoxColumn testingResultDataRow;
    }
}
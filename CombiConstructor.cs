﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CourseWork_tmp1
{
    public partial class CombiConstructor : Form
    {
        CombiPattern combiPattern;
        BindingList<IPattern> blPatterns;
        public CombiConstructor(CombiPattern combiPattern)
        {
            InitializeComponent();

            this.combiPattern = combiPattern;
            btnOK.DialogResult = DialogResult.OK;
            btnOK.Enabled = OpenBtnOk();

            txtName.Text = combiPattern.Name;
            if (combiPattern.PatternList == null)
                combiPattern.PatternList = new List<IPattern>();

            blPatterns = new BindingList<IPattern>(combiPattern.PatternList);
            lbComposition.DataSource = blPatterns;

            PatternLibrary library = PatternLibrary.GetInstance();
            cbxAnswerClasses.DataSource = library.Patterns.Where(pat => !library.IsCombiPattern(pat)).ToList();
            cbxAnswerClasses.DisplayMember = "Name";
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            IPattern selectedItem = (IPattern) cbxAnswerClasses.SelectedItem;
            if (selectedItem != null)
                blPatterns.Add(selectedItem);
            btnOK.Enabled = OpenBtnOk();
        }

        private void btnDel_Click(object sender, EventArgs e)
        {
            var index = lbComposition.SelectedIndex;
            if (index != -1)
                blPatterns.RemoveAt(index);
            btnOK.Enabled = OpenBtnOk();
        }

        private bool OpenBtnOk()
        {
            bool nameOK = !string.IsNullOrEmpty(txtName.Text);
            bool classesOK = !(lbComposition.Items.Count == 0);
            return nameOK && classesOK;
        }

        private void txtName_TextChanged(object sender, EventArgs e)
        {
            btnOK.Enabled = OpenBtnOk();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            combiPattern.Name = txtName.Text;
            combiPattern.PatternList = new List<IPattern>(blPatterns);
        }
    }
}

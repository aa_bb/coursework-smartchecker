﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using CourseWork_tmp1.Model;

namespace CourseWork_tmp1
{
    public partial class TaskEditForm : Form
    {
        Mode mode;
        BindingList<Task> taskRows;
        TaskInfo taskInfo;
        public TaskEditForm(Mode mode, List<Task> taskList)
        {
            InitializeComponent();
            this.mode = mode;
            taskInfo = taskList[0].TaskInfo;
            Text = string.Format("Задача {0}", taskInfo.Name);
            lblTaskName.Text = taskInfo.Name;
            lblTestDirPath.Text = taskInfo.TestDirPath;
            dataViewer.Top = lblTestDirPath.Location.Y + lblTestDirPath.Height + 15;

            taskRows = new BindingList<Task>(taskList);

            dataViewer.DataSource = taskRows;
            for (int i = 3; i < dataViewer.Columns.Count; i++)
                dataViewer.Columns[i].Visible = false;

            dataViewer.Columns[0].HeaderText = "Имя теста";
            dataViewer.Columns[1].HeaderText = "Шаблон";
            dataViewer.Columns[2].HeaderText = "Ограничение по времени";

            dataViewer.Columns[0].ReadOnly = true;
            dataViewer.Columns[1].ReadOnly = true;
            dataViewer.Columns[2].ReadOnly = (mode == Mode.show);

            foreach (DataGridViewColumn c in dataViewer.Columns)
                c.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
        }

        string GetTestContent(int index)
        {
            StreamReader reader = new StreamReader(taskInfo.TestDirPath + "\\" + taskRows[index].Test.Name);
            string res = reader.ReadToEnd();
            reader.Close();
            return res;
        }

        private void dataViewer_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0)
                return;
            int index = e.RowIndex;
            if (e.ColumnIndex == 0)
            {
                StreamReader reader = new StreamReader(taskInfo.TestDirPath + "\\" + taskRows[index].Test.Name);
                MessageBox.Show(reader.ReadToEnd());
                reader.Close();
            }
            else if (e.ColumnIndex == 1 && mode == Mode.edit)
            {
                PatternLibrary library = PatternLibrary.GetInstance();
                int? patternId = taskRows[index].PatternId;
                Pattern pattern;
                using (Context db = new Context())
                {
                    pattern = db.Patterns
                        .Include(p => p.AnswerClass)
                        .Include(p => p.NextOuterPattern)
                        .Include(p => p.NextInnerPattern)
                        .First(x => x.Id == patternId);
                }
                var patternEditForm = new PatternEditForm(pattern);
                if (patternEditForm.ShowDialog() == DialogResult.OK)
                {
                    taskRows[index].Pattern = pattern;
                    List<Pattern> patternCollection = MakePatternCollection(pattern);

                    foreach (var pt in patternCollection)
                    {
                        IPattern iCurpattern = library.Patterns.Where(lac => lac.Name.Equals(pt.AnswerClass.Name)).First();
                        if (pt.AnswerClass != null && library.IsGenPattern(iCurpattern))
                        {
                            int exactMatchId;
                            using (Context db = new Context())
                            {
                                var query = db.AnswerClasses.Where(ac => ac.Name.Equals("Точное совпадение"));
                                if (!query.Any())
                                {
                                    MessageBox.Show("Подключите стандартные плагины");
                                    return;
                                }
                                exactMatchId = (query.First()).Id;
                            }

                            // получить контент из теста
                            string test = GetTestContent(index);
                            string basicAnswer = pt.BasicAnswer;

                            // запустить генерацию
                            List<string> genBasicAnswers = ((IGenPattern)iCurpattern).GenBasicAnswers(test, basicAnswer);

                            // сохранить записи в БД                               
                            Pattern nextPattern = null;
                            using (Context db = new Context())
                            {
                                for (int i = genBasicAnswers.Count - 1; i >= 0; i--)
                                {
                                    string genBA = genBasicAnswers[i];

                                    Pattern newPattern = db.Patterns.Create();
                                    newPattern.AnswerClassId = exactMatchId;
                                    newPattern.BasicAnswer = genBA;
                                    newPattern.Delimiter = null;
                                    db.Patterns.Add(newPattern);
                                    db.SaveChanges();

                                    if (i == 0)
                                        db.Patterns.Find(pt.Id).NextInnerPatternId = newPattern.Id;
                                    newPattern.NextInnerPattern = nextPattern;
                                    nextPattern = newPattern;
                                    db.SaveChanges();
                                }
                            }
                        }
                    }
                }
            }
        }

        List<Pattern> MakePatternCollection(Pattern pattern)
        {
            PatternLibrary library = PatternLibrary.GetInstance();
            IPattern ipattern = library.Patterns.Where(lac => lac.Name.Equals(pattern.AnswerClass.Name)).First();
            List<Pattern> patternCollection = new List<Pattern>();
            if (library.IsCombiPattern(ipattern))
            {
                Pattern curpattern = pattern;
                while (curpattern.NextOuterPattern != null)
                {
                    curpattern = curpattern.NextOuterPattern;
                    patternCollection.Add(curpattern);
                }
            }
            else
                patternCollection.Add(pattern);
            return patternCollection;
        }

        //void DeleteOldPatternRecords(Context db, Pattern dbPattern)
        //{
        //    dbPattern.BasicAnswer = null;
        //    dbPattern.DeleteOldPatternRecords(db);
        //}

        private void dataViewer_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            if (e.ColumnIndex == 2 && e.RowIndex >= 0)
            {
                int index = e.RowIndex;
                int timeLimit;
                if (string.IsNullOrEmpty(e.FormattedValue.ToString()))
                {
                    using (Context db = new Context())
                    {
                        var task = db.Tasks.Find(taskInfo.Id, taskRows[index].Test.Id);
                        if (task.TimeLimit == 0)
                            return;
                        task.TimeLimit = 0;
                        db.SaveChanges();
                    }
                    return;
                }
                if (!int.TryParse(e.FormattedValue.ToString(), out timeLimit))
                {
                    MessageBox.Show("Лимит времени должен быть чиловым значением");
                    e.Cancel = true;
                    return;
                }
                using (Context db = new Context())
                {
                    var task = db.Tasks.Find(taskInfo.Id, taskRows[index].Test.Id);
                    if (task.TimeLimit == timeLimit)
                        return;
                    task.TimeLimit = timeLimit;
                    db.SaveChanges();
                }
            }
        }
    }
}

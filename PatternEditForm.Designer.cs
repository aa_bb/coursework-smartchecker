﻿namespace CourseWork_tmp1
{
    partial class PatternEditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnOkPlain = new System.Windows.Forms.Button();
            this.txtBasicAnswer = new System.Windows.Forms.TextBox();
            this.cbxAnswerClass = new System.Windows.Forms.ComboBox();
            this.grPlainPattern = new System.Windows.Forms.GroupBox();
            this.grCombiPattern = new System.Windows.Forms.GroupBox();
            this.dvCombiPatterns = new System.Windows.Forms.DataGridView();
            this.btnOKCombi = new System.Windows.Forms.Button();
            this.grGenPattern = new System.Windows.Forms.GroupBox();
            this.txtBasicAnswerGen = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnOkGenPattern = new System.Windows.Forms.Button();
            this.grSequencePattern = new System.Windows.Forms.GroupBox();
            this.lbBASeq = new System.Windows.Forms.ListBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnAddSeq = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.txtAnswerSeq = new System.Windows.Forms.TextBox();
            this.btnDelSeq = new System.Windows.Forms.Button();
            this.btnOkSequence = new System.Windows.Forms.Button();
            this.grPlainPattern.SuspendLayout();
            this.grCombiPattern.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dvCombiPatterns)).BeginInit();
            this.grGenPattern.SuspendLayout();
            this.grSequencePattern.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(-5, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(106, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Базовый ответ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(96, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Класс ответа";
            // 
            // btnOkPlain
            // 
            this.btnOkPlain.Location = new System.Drawing.Point(164, 52);
            this.btnOkPlain.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnOkPlain.Name = "btnOkPlain";
            this.btnOkPlain.Size = new System.Drawing.Size(158, 25);
            this.btnOkPlain.TabIndex = 2;
            this.btnOkPlain.Text = "OK";
            this.btnOkPlain.UseVisualStyleBackColor = true;
            this.btnOkPlain.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // txtBasicAnswer
            // 
            this.txtBasicAnswer.Location = new System.Drawing.Point(129, 20);
            this.txtBasicAnswer.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtBasicAnswer.Name = "txtBasicAnswer";
            this.txtBasicAnswer.Size = new System.Drawing.Size(330, 22);
            this.txtBasicAnswer.TabIndex = 3;
            this.txtBasicAnswer.TextChanged += new System.EventHandler(this.txtBasicAnswer_TextChanged);
            // 
            // cbxAnswerClass
            // 
            this.cbxAnswerClass.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxAnswerClass.FormattingEnabled = true;
            this.cbxAnswerClass.Location = new System.Drawing.Point(133, 15);
            this.cbxAnswerClass.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbxAnswerClass.Name = "cbxAnswerClass";
            this.cbxAnswerClass.Size = new System.Drawing.Size(327, 24);
            this.cbxAnswerClass.TabIndex = 4;
            this.cbxAnswerClass.SelectedIndexChanged += new System.EventHandler(this.cbxAnswerClass_SelectedIndexChanged);
            // 
            // grPlainPattern
            // 
            this.grPlainPattern.Controls.Add(this.txtBasicAnswer);
            this.grPlainPattern.Controls.Add(this.label1);
            this.grPlainPattern.Controls.Add(this.btnOkPlain);
            this.grPlainPattern.Location = new System.Drawing.Point(8, 49);
            this.grPlainPattern.Name = "grPlainPattern";
            this.grPlainPattern.Size = new System.Drawing.Size(489, 82);
            this.grPlainPattern.TabIndex = 5;
            this.grPlainPattern.TabStop = false;
            // 
            // grCombiPattern
            // 
            this.grCombiPattern.Controls.Add(this.dvCombiPatterns);
            this.grCombiPattern.Controls.Add(this.btnOKCombi);
            this.grCombiPattern.Location = new System.Drawing.Point(8, 49);
            this.grCombiPattern.Name = "grCombiPattern";
            this.grCombiPattern.Size = new System.Drawing.Size(489, 241);
            this.grCombiPattern.TabIndex = 6;
            this.grCombiPattern.TabStop = false;
            // 
            // dvCombiPatterns
            // 
            this.dvCombiPatterns.AllowUserToAddRows = false;
            this.dvCombiPatterns.AllowUserToDeleteRows = false;
            this.dvCombiPatterns.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dvCombiPatterns.Location = new System.Drawing.Point(-4, 0);
            this.dvCombiPatterns.Name = "dvCombiPatterns";
            this.dvCombiPatterns.RowHeadersWidth = 51;
            this.dvCombiPatterns.RowTemplate.Height = 24;
            this.dvCombiPatterns.Size = new System.Drawing.Size(489, 206);
            this.dvCombiPatterns.TabIndex = 3;
            this.dvCombiPatterns.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dvCombiPatterns_CellValueChanged);
            // 
            // btnOKCombi
            // 
            this.btnOKCombi.Location = new System.Drawing.Point(164, 211);
            this.btnOKCombi.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnOKCombi.Name = "btnOKCombi";
            this.btnOKCombi.Size = new System.Drawing.Size(158, 25);
            this.btnOKCombi.TabIndex = 2;
            this.btnOKCombi.Text = "OK";
            this.btnOKCombi.UseVisualStyleBackColor = true;
            this.btnOKCombi.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // grGenPattern
            // 
            this.grGenPattern.Controls.Add(this.txtBasicAnswerGen);
            this.grGenPattern.Controls.Add(this.label3);
            this.grGenPattern.Controls.Add(this.btnOkGenPattern);
            this.grGenPattern.Location = new System.Drawing.Point(8, 49);
            this.grGenPattern.Name = "grGenPattern";
            this.grGenPattern.Size = new System.Drawing.Size(489, 82);
            this.grGenPattern.TabIndex = 6;
            this.grGenPattern.TabStop = false;
            // 
            // txtBasicAnswerGen
            // 
            this.txtBasicAnswerGen.Location = new System.Drawing.Point(127, 20);
            this.txtBasicAnswerGen.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtBasicAnswerGen.Name = "txtBasicAnswerGen";
            this.txtBasicAnswerGen.Size = new System.Drawing.Size(330, 22);
            this.txtBasicAnswerGen.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(-5, 21);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(106, 17);
            this.label3.TabIndex = 0;
            this.label3.Text = "Базовый ответ";
            // 
            // btnOkGenPattern
            // 
            this.btnOkGenPattern.Location = new System.Drawing.Point(164, 52);
            this.btnOkGenPattern.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnOkGenPattern.Name = "btnOkGenPattern";
            this.btnOkGenPattern.Size = new System.Drawing.Size(158, 25);
            this.btnOkGenPattern.TabIndex = 2;
            this.btnOkGenPattern.Text = "OK";
            this.btnOkGenPattern.UseVisualStyleBackColor = true;
            this.btnOkGenPattern.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // grSequencePattern
            // 
            this.grSequencePattern.Controls.Add(this.lbBASeq);
            this.grSequencePattern.Controls.Add(this.groupBox1);
            this.grSequencePattern.Controls.Add(this.btnDelSeq);
            this.grSequencePattern.Controls.Add(this.btnOkSequence);
            this.grSequencePattern.Location = new System.Drawing.Point(8, 44);
            this.grSequencePattern.Name = "grSequencePattern";
            this.grSequencePattern.Size = new System.Drawing.Size(489, 330);
            this.grSequencePattern.TabIndex = 7;
            this.grSequencePattern.TabStop = false;
            // 
            // lbBASeq
            // 
            this.lbBASeq.FormattingEnabled = true;
            this.lbBASeq.ItemHeight = 16;
            this.lbBASeq.Location = new System.Drawing.Point(-4, 5);
            this.lbBASeq.Name = "lbBASeq";
            this.lbBASeq.Size = new System.Drawing.Size(489, 148);
            this.lbBASeq.TabIndex = 6;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnAddSeq);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.txtAnswerSeq);
            this.groupBox1.Location = new System.Drawing.Point(2, 188);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(477, 95);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Добавить вариант ответа";
            // 
            // btnAddSeq
            // 
            this.btnAddSeq.Location = new System.Drawing.Point(96, 62);
            this.btnAddSeq.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnAddSeq.Name = "btnAddSeq";
            this.btnAddSeq.Size = new System.Drawing.Size(283, 25);
            this.btnAddSeq.TabIndex = 6;
            this.btnAddSeq.Text = "Добавить";
            this.btnAddSeq.UseVisualStyleBackColor = true;
            this.btnAddSeq.Click += new System.EventHandler(this.btnAddSeq_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(2, 29);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 17);
            this.label4.TabIndex = 1;
            this.label4.Text = "Ответ:";
            // 
            // txtAnswerSeq
            // 
            this.txtAnswerSeq.Location = new System.Drawing.Point(60, 30);
            this.txtAnswerSeq.Name = "txtAnswerSeq";
            this.txtAnswerSeq.Size = new System.Drawing.Size(398, 22);
            this.txtAnswerSeq.TabIndex = 0;
            // 
            // btnDelSeq
            // 
            this.btnDelSeq.Location = new System.Drawing.Point(102, 158);
            this.btnDelSeq.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnDelSeq.Name = "btnDelSeq";
            this.btnDelSeq.Size = new System.Drawing.Size(283, 25);
            this.btnDelSeq.TabIndex = 4;
            this.btnDelSeq.Text = "Удалить";
            this.btnDelSeq.UseVisualStyleBackColor = true;
            this.btnDelSeq.Click += new System.EventHandler(this.btnDelSeq_Click);
            // 
            // btnOkSequence
            // 
            this.btnOkSequence.Location = new System.Drawing.Point(164, 292);
            this.btnOkSequence.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnOkSequence.Name = "btnOkSequence";
            this.btnOkSequence.Size = new System.Drawing.Size(158, 25);
            this.btnOkSequence.TabIndex = 2;
            this.btnOkSequence.Text = "OK";
            this.btnOkSequence.UseVisualStyleBackColor = true;
            this.btnOkSequence.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // PatternEditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(516, 381);
            this.Controls.Add(this.grSequencePattern);
            this.Controls.Add(this.grGenPattern);
            this.Controls.Add(this.grCombiPattern);
            this.Controls.Add(this.grPlainPattern);
            this.Controls.Add(this.cbxAnswerClass);
            this.Controls.Add(this.label2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PatternEditForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Редактирование шаблона";
            this.grPlainPattern.ResumeLayout(false);
            this.grPlainPattern.PerformLayout();
            this.grCombiPattern.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dvCombiPatterns)).EndInit();
            this.grGenPattern.ResumeLayout(false);
            this.grGenPattern.PerformLayout();
            this.grSequencePattern.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnOkPlain;
        private System.Windows.Forms.TextBox txtBasicAnswer;
        private System.Windows.Forms.ComboBox cbxAnswerClass;
        private System.Windows.Forms.GroupBox grPlainPattern;
        private System.Windows.Forms.GroupBox grCombiPattern;
        private System.Windows.Forms.Button btnOKCombi;
        private System.Windows.Forms.DataGridView dvCombiPatterns;
        private System.Windows.Forms.GroupBox grGenPattern;
        private System.Windows.Forms.TextBox txtBasicAnswerGen;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnOkGenPattern;
        private System.Windows.Forms.GroupBox grSequencePattern;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnAddSeq;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtAnswerSeq;
        private System.Windows.Forms.Button btnDelSeq;
        private System.Windows.Forms.Button btnOkSequence;
        private System.Windows.Forms.ListBox lbBASeq;
    }
}
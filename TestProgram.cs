﻿using System;
using System.Linq;
using System.Text;
using System.IO;
using System.Diagnostics;
using System.Collections.Generic;
using System.ComponentModel;

namespace CourseWork_tmp1
{
    public class TestProgram
    {
        string programPath = "";
        string programName = "";

        public string LabName { get; private set; }
        public string StudentName { get; private set; }
        public List<(string testName, Model.Pattern pattern,
            string studentAnswer, bool result)> TestResults { get; private set; }

        string errors = "";
        int successTestCount = 0;
        int errorTestCount = 0;

        public TestProgram(string programPath, string programName)
        {
            this.programName = programName;
            this.programPath = programPath;
            TestResults = new List<(string testName, Model.Pattern pattern,
            string studentAnswer, bool result)>();
            if (!splitProgramName())
                errors = "Неверное имя файла \"" + programName + "\"";
        }

        //Разобрать имя исполняемого файла
        bool splitProgramName() //имя имеет вид "<Описатель>_<Имя лабораторной>.exe
        {
            string[] splitName = programName.Split(new char[] { '_', '.' });
            int lastElem = splitName.Count() - 1;
            if (lastElem < 2)
                return false;
            LabName = splitName[lastElem - 1];
            StudentName = splitName[lastElem - 2];

            return true;
        }

        bool cmpFiles(string fileName1, string fileName2, ref string error)
        {
            try
            {
                StreamReader file1 = new StreamReader(fileName1, Encoding.GetEncoding(1251));
                StreamReader file2 = new StreamReader(fileName2, Encoding.GetEncoding(1251));

                bool is_ok = true;

                while (!file1.EndOfStream && !file2.EndOfStream && is_ok)
                {
                    string str1 = file1.ReadLine().Trim().ToLower();
                    string str2 = file2.ReadLine().Trim().ToLower();

                    is_ok = str1 == str2;
                }

                if (is_ok && !file1.EndOfStream)
                {
                    is_ok = false;
                    error = " Не хватает данных";
                }
                is_ok = is_ok && file1.EndOfStream;
                if (is_ok && !file2.EndOfStream)
                {
                    while (is_ok && !file2.EndOfStream)
                    {
                        string str2 = file2.ReadLine().Trim();
                        is_ok = str2 == "";
                    }
                    if (!is_ok)
                    {
                        error = " Лишние данные";
                    }
                }

                file1.Close();
                file2.Close();

                return is_ok;
            }
            catch
            {
                error = " Нет доступа к файлу вывода :( Кто-то не отпустил файл";
                return false;
            }
        }

        //Получить путь к папке с тестами и прогнать все тесты, результат записать в result
        public string testProgram()
        {
            if (errors != "")
                return errors;

            var testFiles = new List<string>();
            var patterns = new List<Model.Pattern>();
            var timeLimits = new List<int>();
            string testDir;
            using (Context db = new Context())
            {
                if (db.TaskInfos.Count() == 0) {
                    errors = "Задачи не были добавлены";
                    return errors;
                }
                if (db.TaskInfos.Where(t => t.Name.Trim().ToLower().Equals(LabName.Trim().ToLower())).Count() == 0)
                {
                    errors = "Такой задачи нет";
                    return errors;
                }
                var taskInfo = db.TaskInfos.Where(t => t.Name.Trim().ToLower().Equals(LabName.Trim().ToLower())).First();
                var taskId = taskInfo.Id;
                testDir = taskInfo.TestDirPath;
                if (db.Tasks.Count() == 0)
                {
                    errors = "Тесты не были добавлены";
                    return errors;
                }
                var tasks = db.Tasks.Where(t => t.TaskInfoId == taskId);
                foreach (var task in tasks)
                {
                    testFiles.Add(db.Tests.Find(task.TestId).Name);
                    patterns.Add(db.Patterns.Find(task.PatternId));
                    timeLimits.Add(task.TimeLimit);
                }
            }

            string fullProgramName = Path.Combine(programPath, programName);
            string panDirectory = "Pan";
            File.Copy(fullProgramName, Path.Combine(panDirectory, programName), true);
            Process currentProcess = null;

            int testCount = 0;
            for (int i = 0; i < testFiles.Count() + 1; i++)  //Для каждого файла среди тестовых
            {
                string log = "";
                var txtFiles = Directory.GetFiles(panDirectory, "*.txt");
                if (currentProcess != null)
                {
                    try
                    {
                        currentProcess.Kill();
                    }
                    catch (InvalidOperationException) { }
                    catch (Exception)
                    {
                        log = " Проблемы при завершении текущего процесса ";
                        return (errors += "Приложение не может быть завершено корректно");
                    }
                }
                foreach (var file in txtFiles)
                    File.Delete(file);
                if (i == testFiles.Count)
                    break;

                string testPath = testFiles[i];
                Model.Pattern pattern = patterns[i];
                if (pattern == null || pattern.Id == 0 || pattern.AnswerClassId == 0)
                    continue;
                int waitingTime = timeLimits[i] * 1000;

                try
                {
                    File.Copy(Path.Combine(testDir, testPath), Path.Combine(panDirectory, Path.GetFileName(testPath)), true);
                    File.Move(Path.Combine(panDirectory, Path.GetFileName(testPath)), Path.Combine(panDirectory, "input.txt"));
                } catch (Exception)
                {
                    log += "Файл теста не найден";
                    continue;
                }

                //Запустить программу
                currentProcess = new Process();
                currentProcess.StartInfo.FileName = Path.Combine(Path.GetFullPath(panDirectory), programName);
                currentProcess.StartInfo.WorkingDirectory = Path.GetFullPath(panDirectory);
                currentProcess.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                currentProcess.Start();
                string ProcessName = currentProcess.ProcessName;

                //Дождаться окончание выполнения прораммы
                if (waitingTime != 0)
                    currentProcess.WaitForExit(waitingTime);
                else
                    currentProcess.WaitForExit();

                if (!File.Exists(Path.Combine(panDirectory, "output.txt")))
                {
                    if (waitingTime != 0) //При работе выход за время
                        log = "Выход за допустимое время работы";
                    else
                        log = "Файл с результатом работы не был создан";

                    TestResults.Add((testName: testPath, pattern: pattern, studentAnswer: log, result: false));
                    continue;
                }

                string studentAnswer;
                try
                {
                    var reader = new StreamReader(Path.Combine(panDirectory, "output.txt"));
                    studentAnswer = reader.ReadToEnd();
                    reader.Close();
                }
                catch (Exception)
                {
                    log += "Файл не был закрыт";
                    continue;
                }

                // генерация и сравнение ответа по шаблону
                bool isAnswerCorrect;
                using (Context db = new Context())
                {
                    string answerClass = db.AnswerClasses.Find(pattern.AnswerClassId).Name;
                    IPattern lexem = PatternLibrary.GetInstance().Patterns.Find(p => p.Name == answerClass);
                    if (lexem == null)
                    {
                        errors = string.Format("Шаблон {0} не загружен, проверьте подключенные dll", answerClass);
                        return errors;
                    }
                    
                    if (PatternLibrary.GetInstance().IsCombiPattern(lexem))
                    {
                        List<string> delimiters = new List<string>();
                        List<List<string>> basicAnswers = new List<List<string>>();
                        int? nextOuterPatternId = pattern.NextOuterPatternId;
                        int? nextInnerPatternId = pattern.NextInnerPatternId;
                        do
                        {
                            List<string> curBasicAnswers = new List<string>();
                            Model.Pattern curPattern = db.Patterns.Find(nextOuterPatternId);
                            if (curPattern != null)
                            {
                                nextOuterPatternId = curPattern.NextOuterPatternId;
                                nextInnerPatternId = curPattern.NextInnerPatternId;
                                if (nextInnerPatternId == null)
                                    curBasicAnswers.Add(curPattern.BasicAnswer);
                                if (!string.IsNullOrEmpty(curPattern.Delimiter))
                                    delimiters.Add(curPattern.Delimiter);
                                basicAnswers.Add(curBasicAnswers);
                            }
                            while (nextInnerPatternId != null)
                            {
                                Model.Pattern curInnerPattern = db.Patterns.Find(nextInnerPatternId);
                                nextInnerPatternId = curInnerPattern.NextInnerPatternId;
                                curBasicAnswers.Add(curInnerPattern.BasicAnswer);
                            }
                        } while (nextOuterPatternId != null);

                        List<string> parsedStudentAnswers = new List<string>();

                        try
                        {
                            foreach (string d in delimiters)
                            {
                                char[] dels;
                                if (d == "\\r\\n")
                                    dels = new char[] { '\r', '\n' };
                                else dels = new char[] { d[0] };
                                string[] strs = studentAnswer.Split(dels);
                                parsedStudentAnswers.Add(strs[0]);
                                studentAnswer = studentAnswer.Remove(0, strs[0].Length);
                                studentAnswer = studentAnswer.Remove(0, dels.Length);
                            }
                            parsedStudentAnswers.Add(studentAnswer);
                            isAnswerCorrect = ((ICombiPattern)lexem).IsAnswerCorrect(parsedStudentAnswers, basicAnswers);
                        } catch(Exception e)
                        {
                            isAnswerCorrect = false;
                        }
                    }
                    else
                        isAnswerCorrect = lexem.IsAnswerCorrect(new List<string>() { pattern.BasicAnswer }, new List<string>() { studentAnswer });
                }

                if (isAnswerCorrect)
                    successTestCount++;
                else errorTestCount++;
                testCount++;

                try {
                    currentProcess.Kill();
                }
                catch (InvalidOperationException) { }
                catch (Exception)
                {
                    log = " Проблемы при завершении текущего процесса " + ProcessName;
                }

                try { //Удалить результирующий файл
                    File.Delete(Path.Combine(panDirectory, "output.txt"));
                }
                catch {
                    log += " Проблемы при удалении файла с результатом (кто-то забыл закрыть файл, а?) ";
                }
                TestResults.Add((testName: testPath, pattern: pattern, 
                    studentAnswer: studentAnswer + "\n" + log, result: isAnswerCorrect));
            }

            File.Delete(Path.Combine(panDirectory, programName));
            if (testCount == 0) {
                errors = "Ни один тест не был корректно проверен";
                return errors;
            }
            return string.Format("{0}\\{1}", successTestCount, testCount);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Windows.Forms;
using CourseWork_tmp1.Model;

namespace CourseWork_tmp1
{
    public partial class PatternEditForm : Form
    {
        Pattern pattern;
        BindingList<Pattern> seqPatternList = new BindingList<Pattern>();
        int sequenceACId;

        public PatternEditForm(Pattern pattern)
        {
            InitializeComponent();
            this.pattern = pattern;
            var patterns = PatternLibrary.GetInstance().Patterns;

            cbxAnswerClass.DataSource = patterns;
            SetGroupsInvisible();
            DialogResultBtns();

            AutoSize = true;
            AutoSizeMode = AutoSizeMode.GrowAndShrink;

            IEnumerable<IPattern> query;
            if (pattern == null || pattern.AnswerClass == null)
                query = patterns.Where(p => p is EmptyPattern);
            else
                query = patterns.Where(p => p.Name == pattern.AnswerClass.Name);

            if (query.Any())
                cbxAnswerClass.SelectedItem = query.First();
        }

        void DialogResultBtns()
        {
            btnOkPlain.DialogResult = DialogResult.OK;
            btnOKCombi.DialogResult = DialogResult.OK;
            btnOkSequence.DialogResult = DialogResult.OK;
            btnOkGenPattern.DialogResult = DialogResult.OK;
        }

        void SetGroupsInvisible()
        {
            grCombiPattern.Visible = false;
            grGenPattern.Visible = false;
            grSequencePattern.Visible = false;
            grPlainPattern.Visible = false;
        }

        void DeleteOldPatternRecords(Context db, Pattern dbPattern)
        {
            dbPattern.BasicAnswer = null;
            dbPattern.DeleteOldPatternRecords(db);
        }

        void SavePlainPattern(Pattern dbPattern)
        {
            dbPattern.BasicAnswer = txtBasicAnswer.Text;
            dbPattern.NextOuterPattern = null;
            dbPattern.NextInnerPattern = null;
            dbPattern.Delimiter = null;
        }

        void SaveCombiPattern(Context db, Pattern dbPattern)
        {
            // создаем новые записи
            Pattern nextPattern = null;
            for (int i = dvCombiPatterns.Rows.Count - 1; i >= 0; i--)
            {
                DataGridViewRow row = dvCombiPatterns.Rows[i];
                Pattern newPattern = db.Patterns.Create();
                string acName = ((AnswerClass)row.Cells[0].Value).Name;
                int acId = db.AnswerClasses.Where(ac => ac.Name == acName).First().Id;
                newPattern.AnswerClassId = acId;
                newPattern.BasicAnswer = row.Cells[1].Value.ToString();
                newPattern.Delimiter = row.Cells[2].Value == null ? "" : row.Cells[2].Value.ToString();
                db.Patterns.Add(newPattern);
                db.SaveChanges();
                if (i == 0)
                    dbPattern.NextOuterPattern = newPattern;
                newPattern.NextOuterPattern = nextPattern;
                nextPattern = newPattern;
                db.SaveChanges();
            }
        }

        void SaveSequencePattern(Context db, Pattern dbPattern, ISequencePattern sequence)
        {
            // создаем новые записи
            Pattern nextPattern = null;
            for (int i = lbBASeq.Items.Count - 1; i >= 0; i--)
            {
                Pattern item = (Pattern) lbBASeq.Items[i];
                Pattern newPattern = db.Patterns.Create();
                newPattern.AnswerClassId = sequenceACId;
                newPattern.BasicAnswer = item.BasicAnswer;
                newPattern.Delimiter = null;
                db.Patterns.Add(newPattern);
                db.SaveChanges();
                if (i == 0)
                    dbPattern.NextInnerPattern = newPattern;
                newPattern.NextInnerPattern = nextPattern;
                nextPattern = newPattern;
                db.SaveChanges();
            }
        }

        void SaveGenPattern(Pattern dbPattern)
        {
            // обновить данные в начальной записи
            SavePlainPattern(dbPattern);
            dbPattern.BasicAnswer = txtBasicAnswerGen.Text;
        }

        // !!! update (no del) first pattern to save pattern_ID in task
        // while (nextPattern != null) delete pattern from db
        // create new records due interface data
        private void btnOk_Click(object sender, EventArgs e)
        {
            IPattern patternImpl = (IPattern)cbxAnswerClass.SelectedItem;
            PatternLibrary library = PatternLibrary.GetInstance();
            using (Context db = new Context())
            {
                var dbPattern = db.Patterns.Find(pattern.Id);
                DeleteOldPatternRecords(db, dbPattern);

                AnswerClass answerClass = db.AnswerClasses.Where(ac => ac.Name == ((IPattern)cbxAnswerClass.SelectedItem).Name).First();
                dbPattern.AnswerClass = answerClass;
                if (library.IsPlainPattern(patternImpl))
                    SavePlainPattern(dbPattern);

                else if (library.IsCombiPattern(patternImpl))
                    SaveCombiPattern(db, dbPattern);

                else if (library.IsGenPattern(patternImpl))
                    SaveGenPattern(dbPattern);

                else if (library.IsSequencePattern(patternImpl))
                    SaveSequencePattern(db, dbPattern, (ISequencePattern) patternImpl);

                db.SaveChanges();
                SavePattern(dbPattern);
                MessageBox.Show("Шаблон сохранен");
                DialogResult = DialogResult.OK;
                Close();
            }
        }

        void SavePattern(Pattern dbPattern)
        {
            pattern.AnswerClass = dbPattern.AnswerClass;
            pattern.BasicAnswer = dbPattern.BasicAnswer;
            pattern.NextOuterPattern = dbPattern.NextOuterPattern;
            pattern.NextInnerPattern = dbPattern.NextInnerPattern;
            pattern.Delimiter = dbPattern.Delimiter;
        }

        bool OpenOKButton()
        {
            bool correctSelectedIndex = cbxAnswerClass.SelectedIndex != -1;
            if (!correctSelectedIndex)
                return false;
            IPattern patternImpl = (IPattern)cbxAnswerClass.SelectedItem;

            if (PatternLibrary.GetInstance().IsPlainPattern(patternImpl))
                return !string.IsNullOrEmpty(txtBasicAnswer.Text);
            if (PatternLibrary.GetInstance().IsCombiPattern(patternImpl))
                return CombiPatternOpenOKCondition();
            if (PatternLibrary.GetInstance().IsGenPattern(patternImpl))
            {
                bool notEmptyBAGen = !string.IsNullOrEmpty(txtBasicAnswerGen.Text);
                return notEmptyBAGen;
            }
            if (PatternLibrary.GetInstance().IsSequencePattern(patternImpl))
            {
                bool notEmptyBASeq = lbBASeq.Items.Count != 0;
                return notEmptyBASeq;
            }
            return false;
        }

        bool CombiPatternOpenOKCondition()
        {
            bool notEmptyBACombi = true;
            for (int i = 0; notEmptyBACombi && i < dvCombiPatterns.Rows.Count; i++)
            {
                object value = dvCombiPatterns.Rows[i].Cells[1].Value;
                if (value == null || string.IsNullOrEmpty(value.ToString()))
                    notEmptyBACombi = false;
            }

            bool notEmptyDelimiter = true;
            for (int i = 0; notEmptyDelimiter && i < dvCombiPatterns.Rows.Count - 1; i++)
            {
                object value = dvCombiPatterns.Rows[i].Cells[2].Value;
                if (value == null || string.IsNullOrEmpty(value.ToString()))
                    notEmptyBACombi = false;
            }
            return notEmptyBACombi && notEmptyDelimiter;
        }

        private void cbxAnswerClass_SelectedIndexChanged(object sender, EventArgs e)
        {
            IPattern patternImpl = (IPattern)cbxAnswerClass.SelectedItem;
            SetGroupsInvisible();
            PatternLibrary library = PatternLibrary.GetInstance();

            // показываем группу
            // заполняем данными из БД

            if (library.IsPlainPattern(patternImpl))
                PlainPatternSelected();

            else if (library.IsCombiPattern(patternImpl))
                CombiPatternSelected((ICombiPattern)patternImpl);

            else if (library.IsGenPattern(patternImpl))
                GenPatternSelected();

            else if (library.IsSequencePattern(patternImpl))
                SeqPatternSelected((ISequencePattern)patternImpl);
            
            btnOKCombi.Enabled = OpenOKButton();
        }

        void PlainPatternSelected()
        {
            grPlainPattern.Visible = true;
            txtBasicAnswer.Text = pattern.BasicAnswer;
        }

        void CombiPatternSelected(ICombiPattern combi)
        {
            grCombiPattern.Visible = true;
            BindingList<Pattern> patternList = new BindingList<Pattern>();
            using (Context db = new Context())
            {
                int? nextPatternId = pattern.NextOuterPatternId;
                if (nextPatternId == null)
                {
                    // создаем объекты при смене класса
                    List<IPattern> answerClasses = combi.PatternList;
                    for (int i = 0; i < answerClasses.Count; i++)
                    {
                        IPattern ac = answerClasses[i];
                        string delimiter = (i == answerClasses.Count - 1) ? null : @"\r\n";
                        AnswerClass answerClass = db.AnswerClasses.Where(anc => anc.Name == ac.Name).First();
                        Pattern curPattern = new Pattern() { AnswerClass = answerClass, BasicAnswer = null, Delimiter = delimiter };
                        patternList.Add(curPattern);
                    }
                }
                else
                {
                    // достаем из БД при загрузке формы
                    while (nextPatternId != null)
                    {
                        Pattern curPattern = db.Patterns
                            .Include(p => p.NextOuterPattern)
                            .Include(p => p.NextInnerPattern)
                            .Include(p => p.AnswerClass)
                            .First(x => x.Id == nextPatternId);
                        patternList.Add(curPattern);
                        nextPatternId = curPattern.NextOuterPatternId;
                    }
                }
            }

            FillCombiDataGridView(patternList);
        }

        void GenPatternSelected()
        {
            grGenPattern.Visible = true;
            txtBasicAnswerGen.Text = pattern.BasicAnswer;
        }

        void SeqPatternSelected(ISequencePattern sequence)
        {
            grSequencePattern.Visible = true;

            using (Context db = new Context())
            {
                int? nextInnerPatternId = pattern.NextInnerPatternId;
                if (nextInnerPatternId == null) // создание новых объектов
                {
                    sequence.PatternList = new List<IPlainPattern>();
                    AnswerClass answerClass = db.AnswerClasses.Where(anc => anc.Name.Equals("Точное совпадение")).First();
                    sequenceACId = answerClass.Id;
                }
                else // подгрузка из БД
                {
                    while (nextInnerPatternId != null)
                    {
                        Pattern curPattern = db.Patterns
                            .Include(p => p.NextOuterPattern)
                            .Include(p => p.NextInnerPattern)
                            .Include(p => p.AnswerClass)
                            .First(x => x.Id == nextInnerPatternId);
                        seqPatternList.Add(curPattern);
                        nextInnerPatternId = curPattern.NextInnerPatternId;
                    }
                }

                lbBASeq.DataSource = seqPatternList;
                lbBASeq.DisplayMember = "BasicAnswer";
            }
        }
        void FillCombiDataGridView(BindingList<Pattern> patternList)
        {
            dvCombiPatterns.DataSource = patternList;
            for (int i = 3; i < dvCombiPatterns.ColumnCount; i++)
                dvCombiPatterns.Columns[i].Visible = false;
            dvCombiPatterns.Columns[0].HeaderText = "Класс ответа";
            dvCombiPatterns.Columns[1].HeaderText = "Базовый ответ";
            dvCombiPatterns.Columns[2].HeaderText = "Разделитель";
            dvCombiPatterns.Columns[0].ReadOnly = true;
            foreach (DataGridViewColumn c in dvCombiPatterns.Columns)
                c.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
        }

        private void txtBasicAnswer_TextChanged(object sender, EventArgs e)
        {
            btnOkPlain.Enabled = OpenOKButton();
        }

        private void btnAddSeq_Click(object sender, EventArgs e)
        {
            OpenOKButton();
            if (!string.IsNullOrEmpty(txtAnswerSeq.Text))
            {
                Pattern newPattern = new Pattern() { AnswerClassId = sequenceACId, BasicAnswer = txtAnswerSeq.Text, Delimiter = null };
                seqPatternList.Add(newPattern);
                
            }
        }

        private void btnDelSeq_Click(object sender, EventArgs e)
        {
            OpenOKButton();
            if (lbBASeq.SelectedItem != null)
                seqPatternList.Remove((Pattern) lbBASeq.SelectedItem);
        }

        private void dvCombiPatterns_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            btnOKCombi.Enabled = OpenOKButton();
        }
    }
}

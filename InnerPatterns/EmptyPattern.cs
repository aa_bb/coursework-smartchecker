﻿using System;
using System.Collections.Generic;

namespace CourseWork_tmp1
{
    class EmptyPattern : IPattern
    {
        public string Name { get { return "Не задано"; } }

        public bool IsAnswerCorrect(List<string> studentAnswers, List<string> basicAnswers)
        {
            throw new NotImplementedException();
        }

        public override string ToString()
        {
            return Name;
        }
    }
}

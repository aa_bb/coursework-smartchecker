﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourseWork_tmp1
{
    public class CombiPattern : ICombiPattern
    {
        public CombiPattern() { }
        public List<IPattern> PatternList { get; set; }

        public string Name { get; set; }

        public bool IsAnswerCorrect(List<string> studentAnswers, List<List<string>> basicAnswers)
        {
            for (int i = 0; i < PatternList.Count; i++)
                if (!PatternList[i].IsAnswerCorrect(new List<string>() { studentAnswers[i] }, basicAnswers[i]))
                    return false;
            return true;
        }

        public bool IsAnswerCorrect(List<string> studentAnswers, List<string> basicAnswers)
        {
            throw new NotImplementedException();
        }

        public override string ToString()
        {
            return Name;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CourseWork_tmp1
{
    public partial class PluginManagerForm : Form
    {
        public PluginManagerForm()
        {
            InitializeComponent();
            foreach (var dllName in PatternLibrary.GetInstance().DllList)
            {
                if (!PatternLibrary.GetInstance().PatternsDict.ContainsKey(dllName))
                    dataViewer.Rows.Add(dllName, "", false, GetButtonText(false));
                else 
                    dataViewer.Rows.Add(dllName, GetLexems(dllName), true, GetButtonText(true));
            }
        }

        string GetLexems(string dllName)
        {
            StringBuilder lexems = new StringBuilder();
            PatternLibrary.GetInstance().PatternsDict[dllName].ForEach(l => lexems.Append(l.Name + " "));
            return lexems.ToString();
        }

        string GetButtonText(bool isActive)
        {
            return isActive ? "Отключить" : "Включить";
        }

        private void btnLoadPlugin_Click(object sender, EventArgs e)
        {
            openFileDialog1.Filter = "dll files(*.dll) | *.dll";
            if (openFileDialog1.ShowDialog() == DialogResult.Cancel)
                return;
            string fileName = openFileDialog1.FileName;
            txtPluginPath.Text = fileName;
            txtPluginPath.Enabled = false;
            btnLoadPlugin.Enabled = false;

            // тут удалить, ниже раскомментировать
            PatternLibrary.GetInstance().LoadPatternsFromDll(fileName);

            dataViewer.Rows.Add(fileName, GetLexems(fileName), true, GetButtonText(true));
            //if (PatternLibrary.GetInstance().LoadPatternsFromDll(fileName) == 0)
            //    MessageBox.Show("Выбранная dll не содержит лексем или уже добавлена");
            //else
            //{
                //ПЕРЕПИСАТЬ ВСЕ К ЧЕРТОВОЙ МАТЕРИ
                //using (Context db = new Context())
                //{
                //    db.Plugins.Add(new Model.Plugin() { Path = fileName });
                //    db.SaveChanges();
                //}

                //dataViewer.Rows.Add(fileName, GetLexems(fileName), true, GetButtonText(true));
            //}
            txtPluginPath.Enabled = true;
            btnLoadPlugin.Enabled = true;
        }

        private void dataViewer_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex != 3)
                return;
            string dllName = dataViewer[0, e.RowIndex].Value.ToString();

            //включить
            if (dataViewer[e.ColumnIndex, e.RowIndex].Value.ToString().Equals(GetButtonText(false)))
            {
                dataViewer[e.ColumnIndex, e.RowIndex].Value = GetButtonText(true);
                dataViewer[2, e.RowIndex].Value = true;
                PatternLibrary.GetInstance().LoadPatternsFromDll(dllName);
            }
            //отключить
            else if (dataViewer[e.ColumnIndex, e.RowIndex].Value.ToString().Equals(GetButtonText(true)))
            {
                dataViewer[e.ColumnIndex, e.RowIndex].Value = GetButtonText(false);
                dataViewer[2, e.RowIndex].Value = false;
                PatternLibrary.GetInstance().ExcludePatterns(dllName);
            }
        }
    }
}

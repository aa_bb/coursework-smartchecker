﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CourseWork_tmp1
{
    public partial class StudentResultForm : Form
    {
        public StudentResultForm(string studentName, List<(string testName, Model.Pattern pattern, 
            string studentAnswer, bool result)> testResults)
        {
            InitializeComponent();
            Text = "Результаты студента " + studentName;
            foreach(var result in testResults)
            {
                string patternStr = result.pattern == null || result.pattern.Id == 0 ? 
                    "Не задано" : result.pattern.BasicAnswer;
                string answerClassStr;
                if (result.pattern == null || result.pattern.Id == 0)
                    answerClassStr = "Не задано";
                else {
                    using (Context db = new Context()) {
                        var answerClass = db.AnswerClasses.Find(result.pattern.AnswerClassId);
                        answerClassStr = answerClass == null ? "Не задано" : answerClass.Name;
                    }
                }

                var jury = result.result ? "Верно" : "Неверно";
                dataViewer.Rows.Add(result.testName, patternStr, answerClassStr, 
                    result.studentAnswer, jury);
                var lastRow = dataViewer.Rows[dataViewer.Rows.Count - 1];
                foreach (DataGridViewCell cell in lastRow.Cells)
                {
                    if (result.result)
                        cell.Style.BackColor = Color.FromArgb(255, 153, 221, 204);
                    else
                        cell.Style.BackColor = Color.Crimson;
                    //cell.Style.BackColor = Color.FromArgb(255, 244, 189, 174);
                } 
            }
        }
    }
}

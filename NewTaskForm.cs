﻿using System;
using System.IO;
using System.Windows.Forms;

namespace CourseWork_tmp1
{
    public partial class NewTaskForm : Form
    {
        Model.TaskInfo taskInfo;
        const int DefaultTimeLimit = 2;

        public NewTaskForm(Model.TaskInfo taskInfo)
        {
            InitializeComponent();
            this.taskInfo = taskInfo;
            txtTaskName.Text = taskInfo.Name;
            txtPath.Text = taskInfo.TestDirPath;
            taskInfo.DefaultTimeLimit = DefaultTimeLimit;
            txtDefaultTimeLimit.Text = taskInfo.DefaultTimeLimit.ToString();
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            var folderBD = new FolderBrowserDialog();
            folderBD.SelectedPath = txtPath.Text;
            DialogResult dr = folderBD.ShowDialog();
            if (dr == DialogResult.OK)
                txtPath.Text = folderBD.SelectedPath;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtPath.Text) || string.IsNullOrEmpty(txtTaskName.Text))
            {
                MessageBox.Show("Имя и путь к папке тестов должны быть указаны");
                return;
            }

            if (!Directory.Exists(txtPath.Text))
            {
                MessageBox.Show("Папки по указанному пути не существует");
                return;
            }

            int timeLimit;
            if (!int.TryParse(txtDefaultTimeLimit.Text, out timeLimit))
            {
                MessageBox.Show("Лимит времени должен быть целочисленным");
                return;
            }

            taskInfo.Name = txtTaskName.Text;
            taskInfo.TestDirPath = txtPath.Text;
            taskInfo.DefaultTimeLimit = timeLimit;
            DialogResult = DialogResult.OK;
        }
    }
}

﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CourseWork_tmp1.Model
{
    public class Test
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }

        [Column("test_name")]
        public string Name { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}

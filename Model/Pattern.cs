﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;

namespace CourseWork_tmp1.Model
{
    public class Pattern
    {
        [JsonIgnore]
        [ForeignKey("AnswerClassId")]
        public virtual AnswerClass AnswerClass { get; set; }

        [Column("basic_answer")]
        public string BasicAnswer { get; set; }

        [Column("delimiter")]
        public string Delimiter { get; set; }

        [Key]
        [Column("id")]
        public int Id { get; set; }
        
        [Column("answer_class_id")]
        public int? AnswerClassId { get; set; }
        
        [Column("next_inner_pattern_id")]
        public int? NextInnerPatternId { get; set; }

        [Column("next_outer_pattern_id")]
        public int? NextOuterPatternId { get; set; }

        [JsonIgnore]
        [ForeignKey("NextOuterPatternId")]
        public virtual Pattern NextOuterPattern { get; set; }

        [JsonIgnore]
        [ForeignKey("NextInnerPatternId")]
        public virtual Pattern NextInnerPattern { get; set; }

        public override string ToString()
        {
            if (Id == 0 || AnswerClass == null)
                return "Не задано";
            if (AnswerClass.IsCombi == true)
            {
                IPattern patternImpl = PatternLibrary.GetInstance().Patterns.Where(p => p.Name == AnswerClass.Name).First();
                StringBuilder sb = new StringBuilder();
                sb.Append(string.Format("\"{0}\": ", AnswerClass.Name));
                foreach (IPattern pattern in ((ICombiPattern)patternImpl).PatternList)
                    sb.Append(string.Format("\"{0}\", ", pattern.Name));
                return sb.ToString();
            }
                
            return string.Format("\"{0}\" \"{1}\"", AnswerClass.Name, BasicAnswer);
        }

        public void DeleteOldPatternRecords(Context db)
        {
            //int? nextOuterPatternId = NextOuterPatternId;
            //while (nextOuterPatternId != null)
            //{
            //    Pattern curPattern = db.Patterns.Find(nextOuterPatternId);
            //    nextOuterPatternId = curPattern.NextOuterPatternId;
            //    int? nextInnerPatternId = curPattern.NextInnerPatternId;
            //    while (nextInnerPatternId != null)
            //    {
            //        Pattern curInnerPattern = db.Patterns.Find(nextInnerPatternId);
            //        nextInnerPatternId = curInnerPattern.NextInnerPatternId;
            //        db.Patterns.Remove(curInnerPattern);
            //    }
            //    db.Patterns.Remove(curPattern);
            //}

            int? nextOuterPatternId = NextOuterPatternId;
            int? nextInnerPatternId = NextInnerPatternId;
            do
            {
                while (nextInnerPatternId != null)
                {
                    Pattern curInnerPattern = db.Patterns.Find(nextInnerPatternId);
                    nextInnerPatternId = curInnerPattern.NextInnerPatternId;
                    db.Patterns.Remove(curInnerPattern);
                }
                Pattern curPattern = db.Patterns.Find(nextOuterPatternId);
                if (curPattern != null)
                {
                    nextOuterPatternId = curPattern.NextOuterPatternId;
                    db.Patterns.Remove(curPattern);
                }
            } while (nextOuterPatternId != null);
        }
    }
}

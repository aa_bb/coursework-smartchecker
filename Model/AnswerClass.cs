﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace CourseWork_tmp1.Model
{
    public class AnswerClass
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }

        [Column("name")]
        public string Name { get; set; }

        [Column("is_combi")]
        public bool? IsCombi { get; set; }

        
        [Column("plugin_id")]
        public int? PluginId { get; set; }

        [JsonIgnore]
        [ForeignKey("PluginId")]
        public virtual Plugin Plugin { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}

﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CourseWork_tmp1.Model
{
    public class Plugin
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }

        [Column("path")]
        public string Path { get; set; }
    }
}


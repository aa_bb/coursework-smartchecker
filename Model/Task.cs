﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace CourseWork_tmp1.Model
{
    public class Task
    {
        [JsonIgnore]
        [ForeignKey("TestId")]
        public virtual Test Test { get; set; }

        [JsonIgnore]
        [ForeignKey("PatternId")]
        public virtual Pattern Pattern { get; set; }

        [Column("time_limit")]
        public int TimeLimit { get; set; }

        [Key]
        [Column("task_info_id")]
        public int TaskInfoId { get; set; }

        [ForeignKey("TaskInfoId")]
        [JsonIgnore]
        public virtual TaskInfo TaskInfo { get; set; }

        [Key]
        [Column("test_id")]
        public int TestId { get; set; }

        [Column("pattern_id")]
        public int? PatternId { get; set; }
    }
}

﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CourseWork_tmp1.Model
{
    public class TaskInfo
    {
        //public TaskInfo()
        //{
        //    using (Context db = new Context())
        //    {
        //        //init test count
        //    }
        //}
        [Key]
        [Column("id")]
        public int Id { get; set; }

        [Column("task_name")]
        public string Name { get; set; }

        [NotMapped]
        public int TestCount { get; set; }

        [Column("testdir_path")]
        public string TestDirPath { get; set; }

        [Column("default_time_limit")]
        public int DefaultTimeLimit { get; set; }
    }
}

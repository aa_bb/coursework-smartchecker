﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Text.Json;
using Microsoft.VisualBasic;
using CourseWork_tmp1.Model;
using System.ComponentModel;
using System.Data.Entity;

namespace CourseWork_tmp1
{
    public partial class MainForm : Form
    {
        BindingList<TaskInfo> taskInfos;
        public MainForm()
        {
            InitializeComponent();
            var patternLibrary = PatternLibrary.GetInstance();

            FillTaskInfos();
            gvTaskManager.DataSource = taskInfos;
            gvTaskManager.Columns[0].Visible = false;
            gvTaskManager.Columns[1].HeaderText = "Имя";
            gvTaskManager.Columns[2].HeaderText = "Кол-во тестов";
            for (int i = 3; i < gvTaskManager.Columns.Count; i++)
                gvTaskManager.Columns[i].Visible = false;

            CreateButtonColumn("Просмотр");
            CreateButtonColumn("Изменить");
            CreateButtonColumn("Удалить");

            foreach (DataGridViewColumn c in gvTaskManager.Columns)
                c.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
        }

        void CreateButtonColumn(string text)
        {
            DataGridViewButtonColumn col = new DataGridViewButtonColumn();
            col.UseColumnTextForButtonValue = true;
            col.Text = text;
            gvTaskManager.Columns.Add(col);
        }

        void FillTaskInfos()
        {
            taskInfos = new BindingList<TaskInfo>();
            using (Context db = new Context())
            {
                foreach (var taskInfo in db.TaskInfos.ToList())
                {
                    var testCount = db.Tasks.Where(t => t.TaskInfoId == taskInfo.Id).Count();
                    taskInfo.TestCount = testCount;
                    taskInfos.Add(taskInfo);
                }
            }
        }

        private void СоздатьНовуюЗадачуToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CreateNewTask();
        }

        void CreateNewTask()
        {
            using (Context db = new Context())
            {
                var taskInfo = db.TaskInfos.Create();
                db.SaveChanges();
                var result = NewTaksFormFiller(taskInfo);
                if (!result)
                    return;

                var testList = GenTestList(taskInfo);
                taskInfo.TestCount = testList.Count;
                List<Task> taskList = new List<Task>();

                foreach (string testName in testList)
                {
                    Test dbtest = db.Tests.Create();
                    dbtest.Name = testName;
                    var pattern = db.Patterns.Create();
                    pattern = db.Patterns.Add(pattern);
                    db.SaveChanges();
                    var task = new Task()
                    {
                        TaskInfo = taskInfo,
                        Test = dbtest,
                        TimeLimit = taskInfo.DefaultTimeLimit,
                        Pattern = pattern
                    };
                    taskList.Add(task);
                    db.Tasks.Add(task);
                }
                db.SaveChanges();
                taskInfos.Add(taskInfo);

                var taskForm = new TaskEditForm(Mode.edit, taskList);
                taskForm.ShowDialog();
            }
        }

        bool NewTaksFormFiller(TaskInfo taskInfo)
        {
            var newTaskForm = new NewTaskForm(taskInfo);
            var dialogResult = newTaskForm.ShowDialog();
            if (dialogResult != DialogResult.OK)
                return false;

            using (Context db = new Context())
            {
                var count = db.TaskInfos.AsNoTracking().Count(t => t.Name == taskInfo.Name);
                if (count != 0)
                {
                    MessageBox.Show("Задача с таким именем уже существует, измените данные");
                    return NewTaksFormFiller(taskInfo);
                }
            }
            MessageBox.Show("Задача сохранена");
            return true;
        }

        List<string> GenTestList(TaskInfo taskInfo)
        {
            List<string> testList = new List<string>();
            string[] testFiles = Directory.GetFiles(taskInfo.TestDirPath, "*input.txt");
            foreach (var filename in testFiles)
                testList.Add(Path.GetFileName(filename));
            return testList;
        }

        private void btnAddTask_Click(object sender, EventArgs e)
        {
            CreateNewTask();
        }

        private void gvTaskManager_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0)
                return;
            int index = e.RowIndex;
            if (e.ColumnIndex == 0)
            { // view
                ChangeOrShow(index, Mode.show);
            }
            else if (e.ColumnIndex == 1)
            { // change
                ChangeOrShow(index, Mode.edit);
            }
            else if (e.ColumnIndex == 2)
            {  // delete
                using (Context db = new Context())
                {
                    var taskId = taskInfos[index].Id;
                    db.TaskInfos.Remove(db.TaskInfos.Find(taskId));
                    List<Task> tasks = db.Tasks.Where(t => t.TaskInfoId == taskId).ToList();
                    List<Test> tests = new List<Test>();
                    foreach (var task in tasks)
                    {
                        Test test = db.Tests.Find(task.TestId);
                        tests.Add(test);
                        Pattern dbPattern = db.Patterns.Find(task.PatternId);
                        dbPattern.DeleteOldPatternRecords(db);
                        db.Patterns.Remove(dbPattern);
                    }
                    db.Tasks.RemoveRange(tasks);
                    foreach (var t in tests)
                    {
                        db.Tests.Remove(t);
                        db.Entry(t).State = EntityState.Deleted;
                    }
                    db.SaveChanges();
                }
                taskInfos.RemoveAt(index);
            }
        }

        void ChangeOrShow(int index, Mode mode)
        {
            List<Task> taskList = new List<Task>();
            using (Context db = new Context())
            {
                int id = taskInfos[index].Id;
                var query = db.Tasks.Include(t => t.TaskInfo).Include(t => t.Pattern).Include(t => t.Test).Include(t => t.Pattern.AnswerClass);
                taskList = query.Where(t => t.TaskInfoId == id).ToList();
            }
            var tef = new TaskEditForm(mode, taskList);
            tef.Show();
        }

        private void проверитьРешениеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var solutionChecker = new SolutionCheckerForm();
            solutionChecker.Show();
        }

        private void менеджерПлагиновToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var pmf = new PluginManagerForm();
            pmf.Show();
        }

        private void конструкторToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var combiPattern = new CombiPattern();
            var cc = new CombiConstructor(combiPattern);
            var res = cc.ShowDialog();
            if (res == DialogResult.OK)
            {
                string pluginName;
                using (Context db = new Context())
                {
                    AnswerClass answerClass = db.AnswerClasses.Create();
                    answerClass.IsCombi = true;
                    answerClass.Name = combiPattern.Name;
                    answerClass.PluginId = PatternLibrary.EmbeddedId;
                    db.AnswerClasses.Add(answerClass);
                    db.SaveChanges();
                    pluginName = answerClass.Plugin.Path;

                    int? nextId = null;
                    List<IPattern> pl = combiPattern.PatternList;
                    for (int i = pl.Count - 1; i >= 0; i--)
                    {
                        string name = pl[i].Name;
                        AnswerClass pluginAnswerClass = db.AnswerClasses.Where(c => c.Name == name).First();
                        Pattern newPattern = db.Patterns.Add(new Pattern() { AnswerClass = pluginAnswerClass, NextOuterPatternId = nextId });
                        db.SaveChanges();
                        nextId = newPattern.Id;
                    }
                    db.Patterns.Add(new Pattern() { AnswerClass = answerClass, NextOuterPatternId = nextId });
                    db.SaveChanges();
                }
                PatternLibrary.GetInstance().AddPattern(combiPattern, pluginName);
            }
        }

        private void сохранитьЗадачиВФайлToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //using (var fbd = new FolderBrowserDialog())
            //{
            //    fbd.Description = "Выберите папку, в ней будет создан файл сохранения";
            //    DialogResult result = fbd.ShowDialog();
            //    if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
            //    {
            //        string path = fbd.SelectedPath;
            //        var currentDate = DateTime.Now.ToString("dd/MM/yyyy");
            //        string name = Interaction.InputBox("Введите названи файла", "Название", currentDate);
            //        if (string.IsNullOrEmpty(name))
            //            return;
            //        try
            //        {
            //            string fullPath = Path.Combine(path, name) + ".txt";
            //            if (File.Exists(fullPath))
            //            {
            //                string message = "Внимание! Такой файл существует и будет перезаписан!";
            //                string title = "Предупреждение";
            //                MessageBoxButtons buttons = MessageBoxButtons.YesNo;
            //                result = MessageBox.Show(message, title, buttons);
            //                if (result == DialogResult.No)
            //                    MessageBox.Show("Сохранение отменено");
            //                return;
            //            }
            //            using (StreamWriter sw = new StreamWriter(fullPath))
            //            {
            //                using (Context db = new Context())
            //                {
            //                    db.Configuration.ProxyCreationEnabled = false;
            //                    string json = JsonSerializer.Serialize(db.AnswerClasses);
            //                    sw.WriteLine(json);
            //                    json = JsonSerializer.Serialize(db.Plugins);
            //                    sw.WriteLine(json);
            //                    json = JsonSerializer.Serialize(db.Tests);
            //                    sw.WriteLine(json);
            //                    json = JsonSerializer.Serialize(db.Patterns);
            //                    sw.WriteLine(json);
            //                    json = JsonSerializer.Serialize(db.TaskInfos);
            //                    sw.WriteLine(json);
            //                    json = JsonSerializer.Serialize(db.Tasks);
            //                    sw.WriteLine(json);
            //                }
            //            }
            //            MessageBox.Show("Сохранение успешно завершено");
            //        }
            //        catch (Exception ex)
            //        {
            //            MessageBox.Show("Что-то пошло не так");
            //            return;
            //        }
            //    }
            //}
        }

        private void загрузитьЗадачиИзФайлаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //string message = "Это действие приведет к замене данных о задачах в приложении, текущий прогресс может быть потерян. Вы готовы продолжить?";
            //string title = "Загрузить из файла";
            //MessageBoxButtons buttons = MessageBoxButtons.YesNo;
            //DialogResult result = MessageBox.Show(message, title, buttons);
            //if (result == DialogResult.No)
            //    return;
            //var dlg = new OpenFileDialog();
            //dlg.Filter = "Text Files (*.txt)|*.txt|All Files (*.*)|*.*";
            //if (dlg.ShowDialog() != DialogResult.OK)
            //    return;
            //string name = dlg.FileName;
            //try
            //{
            //    using (StreamReader sr = new StreamReader(name))
            //    {
            //        string json = sr.ReadLine();
            //        List<AnswerClass> answerClasses = JsonSerializer.Deserialize<List<AnswerClass>>(json);
            //        json = sr.ReadLine();
            //        List<Plugin> plugins = JsonSerializer.Deserialize<List<Plugin>>(json);
            //        json = sr.ReadLine();
            //        List<Test> tests = JsonSerializer.Deserialize<List<Test>>(json);
            //        json = sr.ReadLine();
            //        List<Pattern> patterns = JsonSerializer.Deserialize<List<Pattern>>(json);
            //        json = sr.ReadLine();
            //        List<TaskInfo> taskInfos = JsonSerializer.Deserialize<List<TaskInfo>>(json);
            //        json = sr.ReadLine();
            //        List<Task> tasks = JsonSerializer.Deserialize<List<Task>>(json);
            //        using (Context db = new Context())
            //        {
            //            var tableNames = new List<string>() { "plugin", "task", "pattern", "answer_class", "test", "task_info" };
            //            foreach (var tableName in tableNames)
            //                db.Database.ExecuteSqlCommand(string.Format("DELETE FROM {0}", tableName));
            //            db.SaveChanges();
            //            foreach (var ac in answerClasses)
            //                db.AnswerClasses.Add(ac);
            //            foreach (var pl in plugins)
            //            {
            //                if (!PatternLibrary.GetInstance().PatternsDict.ContainsKey(pl.Path))
            //                    db.Plugins.Add(pl);
            //            }
            //            foreach (var t in tests)
            //                db.Tests.Add(t);
            //            foreach (var pt in patterns)
            //                db.Patterns.Add(pt);
            //            foreach (var ti in taskInfos)
            //                db.TaskInfos.Add(ti);
            //            foreach (var task in tasks)
            //                db.Tasks.Add(task);
            //            db.SaveChanges();
            //        }
            //        FillTaskInfos();
            //        MessageBox.Show("Загрузка успешно завершена");
            //    }
            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show("Что-то пошло не так\n" + ex);
            //    return;
            //}
        }
    }
}

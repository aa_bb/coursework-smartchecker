﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using CourseWork_tmp1.Model;

namespace CourseWork_tmp1
{
    class PatternLibrary
    {
        public const int EmbeddedId = 1;

        private static PatternLibrary instance;
        public List<IPattern> Patterns { get; set; }
        public Dictionary<string, List<IPattern>> PatternsDict { get; set; }
        public List<string> DllList { get; set; }

        private PatternLibrary()
        {
            Patterns = new List<IPattern>();
            PatternsDict = new Dictionary<string, List<IPattern>>();

            DllList = new List<string>();
            using (Context db = new Context())
            {
                Plugin innerPlugin = null;
                var plugins = db.Plugins;
                foreach (var plugin in plugins)
                {
                    if (!File.Exists(plugin.Path))
                    {
                        if (plugin.Id == EmbeddedId)
                            innerPlugin = plugin;
                        continue;
                    }
                    DllList.Add(@plugin.Path);
                    LoadPatternsFromDll(plugin.Path);
                }
                if (innerPlugin != null)
                    LoadCombiFromDB(innerPlugin);
                AddPattern(new EmptyPattern(), innerPlugin.Path, 0);
            }
        }

        public void AddPattern(IPattern pattern, string pluginName, int index = -1)
        {
            if (index == -1)
                Patterns.Add(pattern);
            else
                Patterns.Insert(index, pattern);

            if (PatternsDict.ContainsKey(pluginName))
                PatternsDict[pluginName].Add(pattern);
            else
                PatternsDict.Add(pluginName, new List<IPattern>() { pattern });
            if (!DllList.Contains(pluginName))
                DllList.Add(pluginName);
        }

        private void LoadCombiFromDB(Plugin plugin)
        {
            using (Context db = new Context())
            {
                List<AnswerClass> answerClasses = db.AnswerClasses.Where(ac => ac.PluginId == plugin.Id).ToList();
                foreach (var answerClass in answerClasses)
                {
                    if (answerClass.IsCombi == true)
                    {
                        var qPatternAC = db.Patterns.Where(p => p.AnswerClassId == answerClass.Id);
                        if (!qPatternAC.Any())
                            return;
                        Pattern answerClassSamplePattern = qPatternAC.First();

                        int? nextId = answerClassSamplePattern.NextOuterPatternId;
                        List<IPattern> patternList = new List<IPattern>();
                        while (nextId != null)
                        {
                            var q1 = db.Patterns.Where(p => p.Id == nextId);
                            if (q1.Any())
                            {
                                Pattern pattern = q1.First();
                                var q2 = Patterns.Where(p => p.Name == pattern.AnswerClass.Name);
                                if (q2.Any())
                                {
                                    IPattern patternFromLibrary = q2.First();
                                    patternList.Add(patternFromLibrary);
                                    nextId = pattern.NextOuterPatternId;
                                }
                            }
                        }
                        CombiPattern combiPattern = new CombiPattern();
                        combiPattern.Name = answerClass.Name;
                        combiPattern.PatternList = patternList;
                        AddPattern(combiPattern, plugin.Path);
                    }
                }
            }
        }

        public static PatternLibrary GetInstance()
        {
            if (instance == null)
                instance = new PatternLibrary();
            return instance;
        }

        public int LoadPatternsFromDll(string dllName)
        {
            if (PatternsDict.ContainsKey(dllName))
                return 0;
            int startPatternsCount = Patterns.Count;
            var dllPatterns = new List<IPattern>();
            try
            {
                var allTypes = Assembly.LoadFile(dllName).GetTypes();
                foreach (var t in allTypes)
                {
                    var i = t.GetInterface("IPattern");
                    if (i == null)
                        continue;
                    foreach (var c in t.GetConstructors())
                        if (c.GetParameters().Length == 0)
                            dllPatterns.Add(c.Invoke(new object[0]) as IPattern);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                return 0;
            }
            //if (Patterns.Count == 0 || Patterns.Count != 0 && Patterns.Count - startPatternsCount != 0)
            //{
            PatternsDict.Add(dllName, dllPatterns);
            Patterns.AddRange(dllPatterns);
            using (Context db = new Context())
            {
                var pluginQuery = db.Plugins.Where(p => p.Path == dllName);
                Plugin plugin;
                if (!pluginQuery.Any())
                {
                    plugin = db.Plugins.Create();
                    plugin.Path = dllName;
                    db.Plugins.Add(plugin);
                    db.SaveChanges();
                }
                else
                    plugin = pluginQuery.First();

                foreach (var p in dllPatterns)
                    if (!db.AnswerClasses.Where(ac => ac.Name == p.Name).Any())
                    {
                        var ac = db.AnswerClasses.Create();
                        ac.IsCombi = false;
                        ac.Name = p.Name;
                        ac.PluginId = plugin.Id;
                        db.AnswerClasses.Add(ac);
                    }
                db.SaveChanges();
            }
            // }
            return Patterns.Count - startPatternsCount;
        }

        public void ExcludePatterns(string dllName)
        {
            List<IPattern> patterns = PatternsDict[dllName];
            patterns.ForEach(ptrn => Patterns.Remove(ptrn));
            PatternsDict.Remove(dllName);
        }

        public bool IsPlainPattern(IPattern pat)
        {
            return pat.GetType().GetInterface(typeof(IPlainPattern).Name) != null;
        }

        public bool IsCombiPattern(IPattern pat)
        {
            return pat.GetType().GetInterface(typeof(ICombiPattern).Name) != null;
        }
        public bool IsSequencePattern(IPattern pat)
        {
            return pat.GetType().GetInterface(typeof(ISequencePattern).Name) != null;
        }
        public bool IsGenPattern(IPattern pat)
        {
            return pat.GetType().GetInterface(typeof(IGenPattern).Name) != null;
        }
    }
}

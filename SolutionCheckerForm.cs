﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace CourseWork_tmp1
{
    public partial class SolutionCheckerForm : Form
    {
        List<List<(string testName, Model.Pattern pattern, 
            string studentAnswer, bool result)>> allSolutionsTestingResults;
        public SolutionCheckerForm()
        {
            InitializeComponent();
            dataViewer.Visible = false;
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            var folderBD = new FolderBrowserDialog();
            folderBD.SelectedPath = txtSolutionsPath.Text;
            DialogResult dr = folderBD.ShowDialog();
            if (dr == DialogResult.OK)
                txtSolutionsPath.Text = folderBD.SelectedPath;
        }

        private void btnStartChecking_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtSolutionsPath.Text))
            {
                MessageBox.Show("Путь к папке решений не указан!");
                return;
            }

            if (!Directory.Exists(txtSolutionsPath.Text))
            {
                MessageBox.Show("Папка не существует!");
                return;
            }
            string[] exeFiles = Directory.GetFiles(txtSolutionsPath.Text, "*.exe");
            if (exeFiles.Count() == 0)
            {
                MessageBox.Show("Папка не содержит исполняемых файлов");
                return;
            }
            dataViewer.Visible = true;
            txtSolutionsPath.Enabled = false;
            btnSelect.Enabled = false;
            btnStartChecking.Enabled = false;

            allSolutionsTestingResults = new List<List<(string testName, Model.Pattern pattern,
            string studentAnswer, bool result)>>();
            foreach (var exe in exeFiles) {
                var tester = new TestProgram(Path.GetDirectoryName(exe), Path.GetFileName(exe));
                var result = tester.testProgram();
                if (!string.IsNullOrEmpty(tester.LabName) && !string.IsNullOrEmpty(tester.StudentName))
                    dataViewer.Rows.Add(tester.StudentName, tester.LabName, result, 
                        allSolutionsTestingResults.Count());
                allSolutionsTestingResults.Add(tester.TestResults);
            }
        }

        private void dataViewer_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0)
                return;
            string studentName = dataViewer[0, e.RowIndex].Value.ToString();
            var index = int.Parse(dataViewer[3, e.RowIndex].Value.ToString());
            var studentResultForm = new StudentResultForm(studentName, allSolutionsTestingResults[index]);
            studentResultForm.Show();
        }
    }
}
